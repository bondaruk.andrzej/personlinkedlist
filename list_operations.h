#ifndef PERSONLINKEDLIST_LIST_OPERATIONS_H
#define PERSONLINKEDLIST_LIST_OPERATIONS_H

void printList(struct Person *head);

void push(struct Person *head, struct Person *newPerson);

void addNewItem(struct Person **head, struct Person *newPerson);

void clearList(struct Person **head);

void removeItemBySurnameAndName(struct Person **head, char name[256], char surname[256]);

int getLength(struct Person *head);

struct Person *searchForItem(struct Person *head, char name[256], char surname[256]);

void sortBySurnameAndName(struct Person **head);

void sortByBirthDate(struct Person **head);

#endif //PERSONLINKEDLIST_LIST_OPERATIONS_H
