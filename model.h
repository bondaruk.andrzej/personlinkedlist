#ifndef PERSONLINKEDLIST_MODEL_H
#define PERSONLINKEDLIST_MODEL_H

typedef enum Flag {
    HAS_KIDS = 1,
    IS_MARRIED = 2,
    HAS_JOB = 4,
    HAS_CAR = 8
} Flag;

struct Person {
    char name[256];
    char surname[256];
    char date[256];
    char residence[256];
    char education[256];
    unsigned int statusFlag;
    struct Person *next;
};

struct Person *newPerson();

struct Person *newPersonFromParams(
        char name[],
        char surname[],
        char date[],
        char residence[],
        char education[],
        unsigned int statusFlag
);

void printModel(struct Person *person);

#endif //PERSONLINKEDLIST_MODEL_H
