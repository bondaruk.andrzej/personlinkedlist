#include <stdio.h>
#include <stdlib.h>
#include "model.h"
#include <string.h>

struct Person *newPersonFromParams(
        char name[],
        char surname[],
        char date[],
        char residence[],
        char education[],
        unsigned int statusFlag
) {
    struct Person *newPerson = malloc(sizeof(struct Person));
    strcpy(newPerson->name, name);
    strcpy(newPerson->surname, surname);
    strcpy(newPerson->date, date);
    strcpy(newPerson->residence, residence);
    strcpy(newPerson->education, education);
    newPerson->statusFlag = statusFlag;
    newPerson->next = NULL;
    return newPerson;
}

unsigned int calculateFlags(int hasKidsFlag, int isMarriedFlag, int hasJobFlag, int hasCarFlag) {
    unsigned int statusFlag = 0;
    if (hasKidsFlag > 0) {
        statusFlag |= HAS_KIDS;
    }
    if (isMarriedFlag > 0) {
        statusFlag |= IS_MARRIED;
    }
    if (hasJobFlag > 0) {
        statusFlag |= HAS_JOB;
    }
    if (hasCarFlag > 0) {
        statusFlag |= HAS_CAR;
    }
    return statusFlag;
}

struct Person *newPerson() {
    printf("Wprowadz dane srudenta do dodania w nastepujacym formacie:\n"
           "Imie[ ]Nazwisko[ ]data urodzenia (RRRRMMDD)[ ]miejsce zamieszkania[ ]wyksztaleceni\n");
    char nameBuffer[256], surnameBuffer[256], dateBuffer[256], residenceBuffer[256], educationBuffer[256];
    scanf("%s%s%s%s%s", nameBuffer, surnameBuffer, dateBuffer, residenceBuffer, educationBuffer);

    printf("Podaj informacje o dodawanej osobie odpowiadajac na ponizsze pytania wprowadzajac\n"
           "0 dla odpowiedzi negatywnej i 1 dla opzytywnej\n\n");
    printf("Czy posiada dzieci? Czy jest po slubie? Czy posiada prace? Czy posiada samochod?\n");
    int hasKidsFlag, isMarriedFlag, hasJobFlag, hasCarFlag;
    scanf("%d%d%d%d\n", &hasKidsFlag, &isMarriedFlag, &hasJobFlag, &hasCarFlag);
    unsigned int statusFlag = calculateFlags(hasKidsFlag, isMarriedFlag, hasJobFlag, hasCarFlag);

    return newPersonFromParams(nameBuffer, surnameBuffer, dateBuffer, residenceBuffer, educationBuffer, statusFlag);
}

unsigned int hasKids(struct Person *person) {
    return (person->statusFlag & HAS_KIDS) > 0;
}

unsigned int isMarried(struct Person *person) {
    return (person->statusFlag & IS_MARRIED) > 0;
}

unsigned int hasJob(struct Person *person) {
    return (person->statusFlag & HAS_JOB) > 0;
}

unsigned int hasCar(struct Person *person) {
    return (person->statusFlag & HAS_CAR) > 0;
}

char *getFlagToText(unsigned int flag) {
    if (flag) {
        return "Tak";
    } else {
        return "Nie";
    }
}

void printModel(struct Person *person) {
    printf("%s %s %s %s %s\n", person->name, person->surname, person->date, person->residence, person->education);
    printf("Dzieci-%s Slub-%s Praca-%s Samochod-%s\n\n",
           getFlagToText(hasKids(person)),
           getFlagToText(isMarried(person)),
           getFlagToText(hasJob(person)),
           getFlagToText(hasCar(person))
    );
}