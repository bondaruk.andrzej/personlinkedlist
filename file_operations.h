#ifndef PERSONLINKEDLIST_FILE_OPERATIONS_H
#define PERSONLINKEDLIST_FILE_OPERATIONS_H

struct DatabaseRowModel {
    char separatedWords[6][256];
};

void saveDatabase(struct Person *head);

void readDatabase(struct Person **head);

#endif //PERSONLINKEDLIST_FILE_OPERATIONS_H
