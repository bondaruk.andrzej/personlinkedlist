#include <stdio.h>
#include "model.h"
#include "list_operations.h"
#include "file_operations.h"

struct Person *head = NULL;

void removeItem() {
    char nameBuffer[256], surnameBuffer[256];
    printf("Podaj dane osoby do usuniecia w formacie: Imie[]Nazwisko\n");
    scanf("%s%s", nameBuffer, surnameBuffer);
    removeItemBySurnameAndName(&head, nameBuffer, surnameBuffer);
}

void searchItem() {
    char nameBuffer[256], surnameBuffer[256];
    printf("Podaj dane szukanej osoby w formacie: Imie[]Nazwisko\n");
    scanf("%s%s", nameBuffer, surnameBuffer);
    struct Person *foundItem = searchForItem(head, nameBuffer, surnameBuffer);
    printModel(foundItem);
}

int main() {
    readDatabase(&head);
    printf("1) Dodaj osobe\n2) Usun osobe\n3) Ile osob\n4) Znajdz osobe\n5) Drukuj liste\n6) Sortuj po "
           "nazwisku i imieniu\n7) Sortuj po roku urodzenia\n8) Zapisz do pliku\n9) Czysc liste\n10) Zakoncz program\n\n");
    int action = -1;
    while (action != 10) {
        scanf("%d", &action);
        switch (action) {
            case 1:
                addNewItem(&head, newPerson());
                break;
            case 2:
                removeItem();
                break;
            case 3:
                printf("Na liscie znajduje sie %d osob.\n", getLength(head));
                break;
            case 4:
                searchItem();
                break;
            case 5:
                printList(head);
                break;
            case 6:
                sortBySurnameAndName(&head);
                break;
            case 7:
                sortByBirthDate(&head);
                break;
            case 8:
                saveDatabase(head);
                break;
            case 9:
                clearList(&head);
                break;
            case 10:
                break;
            default:
                printf("Niepoprawna komenda");
        }
    }
    return 0;
}
