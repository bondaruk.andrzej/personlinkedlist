#include <stdio.h>
#include <stdlib.h>
#include "model.h"
#include "list_operations.h"
#include "file_operations.h"
#include <string.h>

FILE *personDatabase = NULL;

void openDatabase(char *mode) {
    personDatabase = fopen(".\\..\\personDatabase.txt", mode);

    if (personDatabase == NULL) {
        printf("Blad otwarcia bazy danych!");
    }
}

void saveDatabase(struct Person *head) {
    openDatabase("w");
    if (personDatabase == NULL) {
        exit(1);
    }
    struct Person *current = head;
    while (current != NULL) {
        fprintf(personDatabase, "%s %s %s %s %s %d\n",
                current->name,
                current->surname,
                current->date,
                current->residence,
                current->education,
                current->statusFlag
        );
        current = current->next;
    }
    fclose(personDatabase);
}

struct DatabaseRowModel *splitDataLine(char lineBuffer[]) {
    char *stringToken = strtok(lineBuffer, " ");
    int stringPosition = 0;

    struct DatabaseRowModel *model = malloc(sizeof(struct DatabaseRowModel));
    while (stringToken != NULL) {
        strcpy(model->separatedWords[stringPosition], stringToken);
        stringToken = strtok(NULL, " ");
        stringPosition++;
    }
    return model;
}

void readDatabase(struct Person **head) {
    openDatabase("r");
    if (personDatabase == NULL) {
        return;
    }
    int bufferLength = 256;
    char buffer[bufferLength];

    while (fgets(buffer, bufferLength, personDatabase)) {
        struct DatabaseRowModel *model = splitDataLine(buffer);
        addNewItem(head, newPersonFromParams(
                model->separatedWords[0],
                model->separatedWords[1],
                model->separatedWords[2],
                model->separatedWords[3],
                model->separatedWords[4],
                atoi(model->separatedWords[5]))
        );
    }
    fclose(personDatabase);
}