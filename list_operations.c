#include <stdio.h>
#include "model.h"
#include <string.h>
#include <stdlib.h>

void printList(struct Person *head) {
    struct Person *current = head;

    while (current != NULL) {
        printModel(current);
        current = current->next;
    }
}

void push(struct Person *head, struct Person *newPerson) {
    struct Person *current = head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newPerson;
    current->next->next = NULL;
}

void addNewItem(struct Person **head, struct Person *newPerson) {
    if (*head == NULL) {
        *head = newPerson;
    } else {
        push(*head, newPerson);
    }
}

void pop(struct Person **head) {
    struct Person *next = NULL;

    if (*head == NULL) {
        return;
    }

    next = (*head)->next;
    free(*head);
    *head = next;
}

void clearList(struct Person **head) {
    while (*head != NULL) {
        pop(head);
    }
}

int getPositionToRemove(struct Person *head, char name[256], char surname[256]) {
    int position = 0;
    struct Person *current = head;

    while (current != NULL) {
        if (strcmp(current->name, name) == 0 && strcmp(current->surname, surname) == 0) {
            return position;
        }
        position++;
        current = current->next;
    }
    return -1;
}

void removeItemBySurnameAndName(struct Person **head, char name[256], char surname[256]) {
    int currentPosition = 0;
    int positionToRemove = getPositionToRemove(*head, name, surname);
    struct Person *current = *head;
    struct Person *temp = NULL;

    if (positionToRemove < 0) {
        printf("Person o podanym imieniu i nawisku nie istnieje\n");
        return;
    }
    if (positionToRemove == 0) {
        pop(head);
        return;
    }

    for (currentPosition = 0; currentPosition < positionToRemove - 1; currentPosition++) {
        if (current->next == NULL) {
            return;
        }
        current = current->next;
    }

    temp = current->next;
    current->next = temp->next;
    free(temp);
}

int getLength(struct Person *head) {
    int length = 0;
    struct Person *current = head;

    while (current != NULL) {
        length++;
        current = current->next;
    }
    return length;
}

struct Person *searchForItem(struct Person *head, char name[256], char surname[256]) {
    struct Person *current = head;

    while (current != NULL) {
        if (strcmp(current->name, name) == 0 && strcmp(current->surname, surname) == 0) {
            return current;
        }
        current = current->next;
    }
    return NULL;
}

void copyElements(struct Person *destination, struct Person *source) {
    strcpy(destination->name, source->name);
    strcpy(destination->surname, source->surname);
    strcpy(destination->date, source->date);
    strcpy(destination->residence, source->residence);
    strcpy(destination->education, source->education);
    destination->statusFlag = source->statusFlag;
}

void swapElements(struct Person *first, struct Person *second) {
    struct Person *tmp = newPersonFromParams(
            first->name,
            first->surname,
            first->date,
            first->residence,
            first->education,
            first->statusFlag
    );
    copyElements(first, second);
    copyElements(second, tmp);
}

void sortBySurnameAndName(struct Person **head) {
    struct Person *currentElement = *head;
    struct Person *nextElement;

    while (currentElement->next != NULL) {
        nextElement = currentElement->next;
        while (nextElement != NULL) {
            int surnameCompareResult = strcmp(currentElement->surname, nextElement->surname);
            int nameCompareResult = strcmp(currentElement->name, nextElement->name);
            if ((surnameCompareResult == 0 && nameCompareResult > 0) || surnameCompareResult > 0) {
                swapElements(currentElement, nextElement);
            }
            nextElement = nextElement->next;
        }
        currentElement = currentElement->next;
    }
}

void sortByBirthDate(struct Person **head) {
    struct Person *currentElement = *head;
    struct Person *nextElement;

    while (currentElement->next != NULL) {
        nextElement = currentElement->next;
        while (nextElement != NULL) {
            if (atoi(currentElement->date) > atoi(nextElement->date)) {
                swapElements(currentElement, nextElement);
            }
            nextElement = nextElement->next;
        }
        currentElement = currentElement->next;
    }
}